//
//  ComicsListController.swift
//  ComicsBrowser
//
//  UIViewController which fetchs the comics and presents them afterwards
//
//  Created by André Gimenez Faria on 14/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit
import Kingfisher

private struct Static {
    static var token : dispatch_once_t = 0
}

class ComicsListController: UIViewController, ProcessIndicationDelegate, ComicsGridDelegate, DisplaysHeroBackground {
    
    @IBOutlet var backgroundImageView: UIImageView!
    
    @IBOutlet var processIndicationContainerView : UIView!
    
    var processIndicationController : ProcessIndicationController!
    
    var comicsGridController : ComicsGridCollectionViewController!
    
    var selectedComic : Comic!
    
    @IBOutlet var comicsGridContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupHeroBackground()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ComicsListController.handlePreferedHeroChange), name: "preferedHeroChanged", object: nil)
        comicsGridContainerView.alpha = 0
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        dispatch_once(&Static.token, {
            self.process()
        })
        self.navigationController?.navigationBar.toggleTransparency(false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        KingfisherManager.sharedManager.cache.clearMemoryCache()
        KingfisherManager.sharedManager.cache.clearDiskCache()
    }
    
    func process() {
        APIManager.getComics(1, completionHandler: { result, comics in
            if result == .Success {
                self.processIndicationContainerView.hidden = true
                self.comicsGridController.comics = comics
                UIView.animateWithDuration(0.5, animations: {
                    self.comicsGridContainerView.alpha = 1
                })
            }
            else {
                self.processIndicationController.errorMessage = result.rawValue
                self.processIndicationController.indicateFailure()
            }
        })
    }

    func handleComicSelection(comic: Comic) {
        selectedComic = comic
        self.performSegueWithIdentifier("detailSegue", sender: nil)
    }
    
    // Method that reloads the comics after the user preference is changed
    func handlePreferedHeroChange() {
        setupHeroBackground()
        processIndicationContainerView.hidden = false
        comicsGridContainerView.alpha = 0
        comicsGridController.comics = nil
        process()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "processIndicationEmbedSegue" {
            processIndicationController = segue.destinationViewController as! ProcessIndicationController
            processIndicationController.delegate = self
        }
        else if segue.identifier == "comicsGridEmbedSegue" {
            comicsGridController = segue.destinationViewController as! ComicsGridCollectionViewController
            comicsGridController.delegate = self
        }
        else if segue.identifier == "detailSegue" {
            (segue.destinationViewController as! ComicDetailsTableViewController).comic = selectedComic
        }
    }

}
