//
//  Price.swift
//  ComicsBrowser
//
//  Created by André Gimenez Faria on 15/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit
import ObjectMapper

class Price: Mappable {
    
    var amount = 0.0
    
    var type : String?
    
    required init?(_ map: Map) { }
    
    // JSON to Object Mapping
    func mapping(map: Map) {
        amount <- map["price"]
        type <- map["type"]
    }
    
}
