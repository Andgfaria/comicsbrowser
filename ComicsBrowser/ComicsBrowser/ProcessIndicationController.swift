//
//  ProcessIndication.swift
//  ComicsBrowser
//
//  Simple UIViewController to indicate that something is being processed and present a simple retry interface when the process fails
//
//  Created by André Gimenez Faria on 14/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit

protocol ProcessIndicationDelegate {
    func process()
}

class ProcessIndicationController: UIViewController {

    var errorMessage = "" {
        didSet {
            errorMessageLabel?.text = errorMessage
        }
    }
    
    var delegate : ProcessIndicationDelegate!
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var errorMessageLabel: UILabel!
    
    @IBOutlet var tryAgainButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorMessageLabel.text = errorMessage
        tryAgainButton.layer.masksToBounds = true
        tryAgainButton.layer.cornerRadius = 5.0
    }

    private func toggleUIComponents(processing : Bool) {
        activityIndicator.hidden = !processing
        errorMessageLabel.hidden = processing
        tryAgainButton.hidden = processing
    }
    
    func indicateFailure() {
        toggleUIComponents(false)
    }

    @IBAction func tryAgain(sender: AnyObject) {
        toggleUIComponents(true)
        delegate.process()
    }
    
}
