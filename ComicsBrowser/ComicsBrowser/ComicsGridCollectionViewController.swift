//
//  ComicsGridCollectionViewController.swift
//  ComicsBrowser
//
//  CollectionViewController which organizes comics in a simple grid
//
//  Created by André Gimenez Faria on 14/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit

protocol ComicsGridDelegate {
    func handleComicSelection(comic : Comic)
}

class ComicsGridCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var comics : [Comic]? {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    var delegate : ComicsGridDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.registerNib(UINib(nibName: "ComicPreviewCell", bundle: nil), forCellWithReuseIdentifier: "comicPreviewCell")
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return comics?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return ComicPreviewCell.optimalSize
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("comicPreviewCell", forIndexPath: indexPath) as! ComicPreviewCell
        cell.updateWithComic(comics![indexPath.row])
        return cell
    }
    
    // Displays the copyright message at the bottom of the screen
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        return collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "footerView", forIndexPath: indexPath)
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        delegate.handleComicSelection(comics![indexPath.row])
    }
    
}
