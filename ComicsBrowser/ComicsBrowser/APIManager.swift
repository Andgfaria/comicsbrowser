//
//  APIManager.swift
//  MagazinesBrowser
//
//  Created by André Gimenez Faria on 14/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

private enum Constants : String {
    case BASE_URL = "http://gateway.marvel.com:80/v1/public/characters/",
         API_KEY = "bb4470a46d0659a43c566ac6056ed48d",
         HASH = "479474cf0a28eac9998960da4d96f06b"
}

enum APIRequestResult : String {
    case Success = "Success",
         NetworkError = "Network Error",
         MissingJSON = "Missing JSON",
         IncorrectData = "Incorrect Data"
}

class APIManager {
    
    /* Method that fetchs the comics from the given API.
       After the request a completion handler is executed with the request result and a comics array.
       In case of failure, the comics array will be nil
    */
    static func getComics(timeStamp : UInt, completionHandler: ((APIRequestResult,[Comic]?) -> ())?) {
        let url = Constants.BASE_URL.rawValue + "\(SettingsManager.sharedInstance.preferedHero.id)/comics"
        Alamofire.request(.GET, url,
                          parameters: ["ts" : timeStamp, "apikey" : Constants.API_KEY.rawValue, "hash" : Constants.HASH.rawValue])
                 .responseJSON { (response) in
                    if response.result.isSuccess {
                        guard let json = response.result.value as? [String : AnyObject] else { completionHandler?(.MissingJSON,nil); return }
                        guard let magazinesData = json["data"]?["results"] as? [[String : AnyObject]] else { completionHandler?(.IncorrectData,nil); return }
                        let comics = magazinesData.map { Mapper<Comic>().map($0)! }
                                                  .sort({ (a, b) -> Bool in
                                                        a.issueNumber < b.issueNumber
                                                  })
                        completionHandler?(.Success,comics)
                    }
                    else {
                        completionHandler?(.NetworkError,nil)
                    }
                 }
    }
}
