//
//  AboutViewController.swift
//  ComicsBrowser
//
//  View Controller that display personal info
//
//  Created by André Gimenez Faria on 15/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit
import MessageUI

class AboutViewController: UIViewController, MFMailComposeViewControllerDelegate, DisplaysHeroBackground, HasCustomBackButton {

    @IBOutlet var backgroundImageView: UIImageView!
    
    @IBOutlet var profileImageView: UIImageView!
    
    @IBOutlet var callButton: UIButton!
    
    @IBOutlet var mailButton: UIButton!
    
    var phoneURL : NSURL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Makes the profile picture round
        profileImageView.layer.masksToBounds = true
        profileImageView.layer.cornerRadius = 54
        setupButtons()
        setupHeroBackground()
    }
    
    private func setupButtons() {
        // Check if the device can make calls. If it can't, the call button is disabled
        var callButtonShouldBeEnabled = false
        if let url = NSURL(string: "tel://+5511998788481") {
            phoneURL = url
            callButtonShouldBeEnabled = UIApplication.sharedApplication().canOpenURL(url)
        }
        callButton.enabled = callButtonShouldBeEnabled
        mailButton.enabled = MFMailComposeViewController.canSendMail()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupBackButton()
    }
    
    @IBAction func call(sender: AnyObject) {
        if phoneURL != nil {
            UIApplication.sharedApplication().openURL(phoneURL!)
        }
    }
    
    @IBAction func mail(sender: AnyObject) {
        let mailScreen = MFMailComposeViewController()
        mailScreen.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.blackColor()]
        mailScreen.mailComposeDelegate = self
        mailScreen.setSubject("Marvel Comics App")
        mailScreen.setToRecipients(["andfaria13@gmail.com"])
        self.presentViewController(mailScreen, animated: true, completion: nil)
    }
    
    @IBAction func showLinkedIn(sender: AnyObject) {
        if let linkedInURL = NSURL(string: "https://www.linkedin.com/in/andgfaria") {
            UIApplication.sharedApplication().openURL(linkedInURL)
        }
    }
    
    // Method that assures that the compose controller can be dismissed
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

}
