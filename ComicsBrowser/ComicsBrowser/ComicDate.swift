//
//  ComicDate.swift
//  ComicsBrowser
//
//  Created by André Gimenez Faria on 15/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit
import ObjectMapper

class ComicDate: Mappable {
    
    var date : NSDate?
    
    var type : String?
    
    private var ISO80601Transform : TransformOf<NSDate,String> {
        return TransformOf<NSDate,String>(fromJSON:{ (value : String?) -> NSDate? in
            guard let value = value else { return nil }
            return NSDate.dateFromISO8601String(value)
            }, toJSON: { (value : NSDate?) -> String? in
                guard let value = value else { return nil }
                return NSDate.ISO80601StringFromDate(value)
        })
    }
    
    required init?(_ map: Map) { }
    
    // JSON to Object Mapping
    func mapping(map: Map) {
        date <- (map["date"],self.ISO80601Transform)
        type <- map["type"]
    }
    
}
