//
//  SettingsManager.swift
//  ComicsBrowser
//
//  Created by André Gimenez Faria on 16/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit

enum Hero : String {
    
    case CaptainAmerica = "captainamerica",
         Hulk = "hulk",
         IronMan = "ironman",
         SpiderMan = "spiderman"
    
    var id : Int {
        return [CaptainAmerica : 1009220, Hulk: 1009351, IronMan: 1009368, SpiderMan : 1009610][self] ?? 0
    }
    
    var backgroundImage : UIImage? {
        return UIImage(named: "bg_\(self.rawValue)")
    }
    
    var headerImage : UIImage? {
        return UIImage(named: "header_\(self.rawValue)")
    }
    
}

enum AppFonts : String {
    
    case DefaultFont = "TradeGothicW01-Roman",
         BoldDefaultFont = "TradeGothicW01-Bold"
    
    func fontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: self.rawValue, size: size) ?? UIFont.systemFontOfSize(size)
    }
}

class SettingsManager {
    
    static let sharedInstance = SettingsManager()
    
    private init() {
        if let preferedHeroSetting = NSUserDefaults.standardUserDefaults().stringForKey("preferedHero") {
            preferedHero = Hero(rawValue: preferedHeroSetting)
        }
        else {
            preferedHero = .CaptainAmerica
        }
    }
    
    var preferedHero : Hero! {
        didSet {
            NSUserDefaults.standardUserDefaults().setValue(preferedHero.rawValue, forKey: "preferedHero")
            NSUserDefaults.standardUserDefaults().synchronize()
            NSNotificationCenter.defaultCenter().postNotificationName("preferedHeroChanged", object: self)
        }
    }
    
}
