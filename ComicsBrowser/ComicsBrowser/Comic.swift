//
//  Magazine.swift
//  MagazinesBrowser
//
//  Created by André Gimenez Faria on 14/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit
import ObjectMapper

class Comic: Mappable {
    
    var title : String?
    
    var description : String?
    
    var dates : [ComicDate]?
    
    var publishDate : NSDate? {
        let filteredDates = dates?.filter { $0.type == "onsaleDate" }
        return filteredDates?.count > 0 ? filteredDates?[0].date : nil
    }
    
    var issueNumber : Int?
    
    var prices : [Price]?
    
    var printPrice : Double? {
        let filteredPrices = prices?.filter { $0.type == "printPrice" }
        return filteredPrices?.count > 0 ? filteredPrices?[0].amount : nil
    }
    
    var pageCount : UInt?
    
    var thumbnail : Thumbnail?
    
    required init?(_ map: Map) { }
    
    // JSON to Object Mapping
    func mapping(map: Map) {
        title <- map["title"]
        description <- map["description"]
        issueNumber <- map["issueNumber"]
        dates <- map["dates"]
        prices <- map["prices"]
        pageCount <- map["pageCount"]
        thumbnail <- map["thumbnail"]
    }
}
