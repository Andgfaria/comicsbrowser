import UIKit
import ObjectMapper
import Kingfisher

/* The API returns the date in the ISO80601 format
   For convenience, these methods will transform a string in the format into a NSDate object
*/
extension NSDate {
    
    static func dateFromISO8601String(isoString : String) -> NSDate? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return dateFormatter.dateFromString(isoString)
    }
    
    static func ISO80601StringFromDate(date : NSDate) -> String? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return dateFormatter.stringFromDate(date)
    }
    
}

extension UINavigationBar {
    
    func toggleTransparency(transparent : Bool) {
        self.translucent = transparent
        self.setBackgroundImage(transparent ? UIImage.fromColor(UIColor.blackColor().colorWithAlphaComponent(0.5)) : nil, forBarMetrics: UIBarMetrics.Default)
        self.shadowImage = transparent ? UIImage() : nil
    }

    func setCustomBackButton(backButtonImage : UIImage) {
        self.backIndicatorImage = backButtonImage
        self.backIndicatorTransitionMaskImage = backButtonImage
    }
    
}

extension UIImage {
    // Method that returns an image from a given color
    static func fromColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
}

protocol DisplaysHeroBackground {
    func setupHeroBackground()
}

extension DisplaysHeroBackground where Self : UIViewController {
    func setupHeroBackground() {
        if let backgroundImageView = self.valueForKey("backgroundImageView") as? UIImageView {
            backgroundImageView.image = SettingsManager.sharedInstance.preferedHero.backgroundImage
        }
    }
}

extension DisplaysHeroBackground where Self : UITableViewController {
    func setupHeroBackground() {
        self.tableView.backgroundView = UIImageView(image: SettingsManager.sharedInstance.preferedHero.backgroundImage)
        self.tableView.backgroundView?.contentMode = .ScaleAspectFill
    }
}

protocol HasCustomBackButton {
    func setupBackButton()
}

extension HasCustomBackButton where Self : UIViewController {
    func setupBackButton() {
        if let customBackButton = UIImage(named: "ic_back") {
            self.navigationController?.navigationBar.setCustomBackButton(customBackButton)
        }
    }
}

protocol DisplaysComicThumbnail {
    func fetchComicThumbnail(comic : Comic, imageView : UIImageView)
}

extension DisplaysComicThumbnail {
    func fetchComicThumbnail(comic : Comic, imageView : UIImageView) {
            if let imageURL = NSURL(string: (comic.thumbnail?.getImageAbsoluteURL(.Uncanny)) ?? "") {
                imageView.kf_setImageWithURL(imageURL)
            }
    }
}
