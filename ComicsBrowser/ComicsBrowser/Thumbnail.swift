//
//  Thumbnail.swift
//  MagazinesBrowser
//
//  Created by André Gimenez Faria on 14/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit
import ObjectMapper

enum ImageQuality : String {
    case Small = "portrait_small",
         Medium = "portrait_medium",
         XLarge = "portrait_xlarge",
         Fantastic = "portrait_fantastic",
         Incredible = "portrait_incredible",
         Uncanny = "portrait_uncanny"
}

class Thumbnail: Mappable {
    
    var path : String?
    
    var imageExtension : String?

    required init?(_ map: Map) { }
    
    // JSON to Object Mapping
    func mapping(map: Map) {
        path <- map["path"]
        imageExtension <- map["extension"]
    }

    // Method that returns the full url for the thumbnail download
    func getImageAbsoluteURL(quality: ImageQuality) -> String? {
        if self.path != nil && self.imageExtension != nil {
            return path! + "/" + quality.rawValue + "." + imageExtension!
        }
        return nil
    }
}
