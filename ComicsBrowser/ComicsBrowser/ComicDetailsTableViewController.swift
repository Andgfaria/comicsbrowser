//
//  ComicDetailsTableViewController.swift
//  ComicsBrowser
//
//  UITableViewController that shows all info related to a single comic
//
//  Created by André Gimenez Faria on 15/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit
import TextAttributes

class ComicDetailsTableViewController: UITableViewController, DisplaysHeroBackground, HasCustomBackButton, DisplaysComicThumbnail {

    var comic : Comic!
    
    var comicHeaderView : UIView!
    
    @IBOutlet var heroHeaderImageView: UIImageView!
    
    @IBOutlet var thumbnailImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupHeader()
        setupTableView()
    }
    
    private func setupHeader() {
        comicHeaderView = self.tableView.tableHeaderView
        heroHeaderImageView.image = SettingsManager.sharedInstance.preferedHero.headerImage
        fetchComicThumbnail(comic, imageView: thumbnailImageView)
    }
    
    private func setupTableView() {
        setupHeroBackground()
        // Ensures that the table view cells' height will be dinamically generated
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.toggleTransparency(true)
        setupBackButton()
    }
    
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let reuseIdentifier = ["titleCell","descriptionCell","infoCell"][indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath)
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = comic.title
        case 1:
            cell.textLabel?.text = comic.description
        default:
            cell.textLabel?.attributedText = infoAttributedText()
        }
        return cell
    }
    
    // Method that appends the comic publish date, print price and page count into a single string that can be easily displayed
    private func infoAttributedText() -> NSAttributedString {
        let attributedString = NSMutableAttributedString()
        let stringAttributes = TextAttributes().font(AppFonts.BoldDefaultFont.fontWithSize(14))
        if let date = comic.publishDate {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
            attributedString.appendAttributedString(NSAttributedString(string: "Published: ", attributes: stringAttributes))
            attributedString.appendAttributedString(NSAttributedString(string: "\(dateFormatter.stringFromDate(date))\n"))
        }
        if let price = comic.printPrice {
            attributedString.appendAttributedString(NSAttributedString(string: "Price: ", attributes: stringAttributes))
            attributedString.appendAttributedString(NSAttributedString(string: "$\(price)\n"))
        }
        if let pageCount = comic.pageCount {
            attributedString.appendAttributedString(NSAttributedString(string: "Page Count: ", attributes: stringAttributes))
            attributedString.appendAttributedString(NSAttributedString(string: "\(pageCount)\n"))
        }
        return attributedString
    }
    
    override func traitCollectionDidChange(previousTraitCollection: UITraitCollection?) {
        self.tableView.tableHeaderView = self.view.traitCollection.verticalSizeClass != .Compact ? comicHeaderView : nil
        self.tableView.contentInset = self.view.traitCollection.verticalSizeClass != .Compact ? UIEdgeInsetsZero : UIEdgeInsetsMake(56, 0, 0, 0)
    }
    
}
