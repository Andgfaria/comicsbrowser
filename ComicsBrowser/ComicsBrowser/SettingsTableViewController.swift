//
//  SettingsController.swift
//  ComicsBrowser
//
//  A simple UITableViewController where the user can change the prefered hero
//
//  Created by André Gimenez Faria on 16/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit

class SettingsTableviewController: UITableViewController, DisplaysHeroBackground, HasCustomBackButton {
    
    private let options : [Hero] = [.CaptainAmerica,.Hulk,.IronMan,.SpiderMan]

    override func viewDidLoad() {
        super.viewDidLoad()
        setupHeroBackground()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupBackButton()
    }

    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.accessoryType = options[indexPath.row] == SettingsManager.sharedInstance.preferedHero ? .Checkmark : .None
        let selectionView = UIView(frame: cell.frame)
        selectionView.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.25)
        cell.selectedBackgroundView = selectionView
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        SettingsManager.sharedInstance.preferedHero = options[indexPath.row]
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        (view as? UITableViewHeaderFooterView)?.textLabel?.font = AppFonts.BoldDefaultFont.fontWithSize(16)
        (view as? UITableViewHeaderFooterView)?.textLabel?.textColor = UIColor.whiteColor().colorWithAlphaComponent(0.5)
    }

}
