//
//  ComicPreviewCollectionViewCell.swift
//  ComicsBrowser
//
//  Cell which displays the comic thumbnail and the issue number below the thumbnail
//
//  Created by André Gimenez Faria on 14/05/16.
//  Copyright © 2016 AGF. All rights reserved.
//

import UIKit

class ComicPreviewCell: UICollectionViewCell, DisplaysComicThumbnail {
 
    @IBOutlet var thumbnailImageView: UIImageView!
    
    @IBOutlet var issueNumberLabel: UILabel!
    
    static let optimalSize = CGSizeMake(90, 163)
    
    func updateWithComic(comic : Comic) {
        fetchComicThumbnail(comic, imageView: thumbnailImageView)
        issueNumberLabel.text = comic.issueNumber != nil ? "#\(comic.issueNumber!)" : ""
    }
    
}
